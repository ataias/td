#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libnet.h>
#include <pcap.h>
#include <time.h>

//Universidade de Brasília
//Departamento de Ciência da Computação - CIC
//Disciplina: Transmissão de Dados
//Professor: João Gondim
//Trabalho de Implementação
//Aluno: Ataias Pereira Reis; Matrícula 10/0093817


//Como compilar? Considerando que seu sistema tenha libpcap, libnet e gcc instalados:
//gcc -Wall -ggdb -lpcap `libnet-config --defines` `libnet-config --libs` server.c -o server

//Este programa utilizou a libnet e libpcap
//Em relação à libnet, referências online utilizadas incluem:
//https://fossies.org/dox/libnet-libnet-1.2/libnet-functions_8h.html#ac4a33a98186ad71f7ae4c559b039af0f
//http://repolinux.wordpress.com/2011/09/18/libnet-1-1-tutorial/#initializing-and-closing-libnet

//Em relação à libpcap, referência online utilizadas incluem:
//http://www.tcpdump.org/pcap.htm
//http://www.winpcap.org/pipermail/winpcap-users/2007-September/002104.html
// O segundo link foi uma ótima ajuda para elucidar a struct sniff_udp, num momento de grandes dúvidas
//Todos os acessos foram realizados entre 4 e 6 de novembro de 2014

#define SERVER 1
#define CLIENT 0
#define SIZE_ETHERNET 14

//definition for pcap-----------------------
/* Ethernet header */
struct sniff_ethernet {
    u_char ether_dhost[ETHER_ADDR_LEN]; /* Destination host address */
    u_char ether_shost[ETHER_ADDR_LEN]; /* Source host address */
    u_short ether_type; /* IP? ARP? RARP? etc */
};

/* IP header */
//This sniff_struct was obtained at:
//http://www.tcpdump.org/pcap.htm

struct sniff_ip {
    u_char ip_vhl;		/* version << 4 | header length >> 2 */
    u_char ip_tos;		/* type of service */
    u_short ip_len;		/* total length */
    u_short ip_id;		/* identification */
    u_short ip_off;		/* fragment offset field */
#define IP_RF 0x8000		/* reserved fragment flag */
#define IP_DF 0x4000		/* dont fragment flag */
#define IP_MF 0x2000		/* more fragments flag */
#define IP_OFFMASK 0x1fff	/* mask for fragmenting bits */
    u_char ip_ttl;		/* time to live */
    u_char ip_p;		/* protocol */
    u_short ip_sum;		/* checksum */
    u_int32_t ip_src,ip_dst; /* source and dest address */
};
#define IP_HL(ip)		(((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)		(((ip)->ip_vhl) >> 4)

/* UDP header */
struct sniff_udp {
    u_int16_t uh_sport; //source port
    u_int16_t uh_dport; //destination port
    u_int16_t uh_ulen; //udp length
    u_int16_t uh_sum; //udp checksum
};
//UDP_CVT converte de little-endian para big-endian
#define UDP_CVT(V) ((V >> 8) | ((V << 8) & 0xff00))
#define SIZE_UDP        8
//------------------------------------------


// ---------------------------------------Definições de funções ---------------------------------------
int initialize_context(libnet_t **l);
int write_packet(libnet_t *l);
int build_and_write(libnet_t **l, char* payload, u_int32_t payload_s, u_int16_t sp, u_int16_t dp, u_int32_t dest_ip_addr);
u_int32_t getMyIP(libnet_t *l);
int parse(int argc, char *argv[], u_int16_t* sp, u_int16_t* dp, char **ip_addr);
static void client(u_int16_t dp, char *ip_addr_str);
static void server(u_int16_t sp);
void initializeSniffer(char **dev, char *errbuf, u_int16_t sp, pcap_t **handle);
static void disectPacket(u_char *packet, u_int16_t *dp, u_int32_t *dest_ip_addr, u_int8_t **payload);
static void chooseHeadOrTail(char **choice, u_int32_t *S);
//-----------------------------------------------------------------------------------------------------

int main(int argc, char *argv[]) {
    //libnet --------------------------------------
    char *ip_addr_str = NULL; //string with ip of host, in the case of client
    u_int16_t sp, dp; //source and destination ports
    //    ------------------------------------------
    srand(time(NULL)); //to seed rand function
    printf("------Versões testadas das bibliotecas------\n");
    printf("    libpcap versão 1.5.3\n");
    printf("    libnet versão 1.1.6\n\n");
    
    printf("---Versões das bibliotecas no sistema atual---\n");
    printf("    %s\n", pcap_lib_version());
    printf("    %s\n", libnet_version());
    printf("--------------------------------------------\n\n");
    
    
    
    int isServer = parse(argc, argv, &sp, &dp, &ip_addr_str);
    if(isServer) server(sp);
    else if(!isServer) client(dp, ip_addr_str);
    else printf("Um erro ocorreu, tente novamente.\n");
    
    return 0;
}

int initialize_context(libnet_t **l){
	//usar lo0 com 127.0.0.1 de ip para servidor e IP
	//veja se em Linux esse é o mesmo dispositivo ou não
    char errbuf[LIBNET_ERRBUF_SIZE];
    //Inicializando contexto - muder NULL para o nome do dispositivo... no caso, loopback se desejar
    *l = libnet_init(LIBNET_RAW4, NULL, errbuf);
    if(*l == NULL){
        fprintf(stderr, "libnet_init() failed: %s\n", errbuf);
        exit(EXIT_FAILURE);
    }
    return 0;
}

int write_packet(libnet_t *l){
    int bytes_written = libnet_write(l);
    //if(bytes_written != -1) printf("%d bytes written.\n", bytes_written);
    if(bytes_written == -1)  {
        fprintf(stderr, "Error writing packet: %s\n",\
                libnet_geterror(l));
        exit(EXIT_FAILURE);
    }
    return 0;
}

//build_and_write: builds udp and ip headers, then sends sends datagram
int build_and_write(libnet_t **l, char* payload, u_int32_t payload_s, u_int16_t sp, u_int16_t dp, u_int32_t dest_ip_addr){
    //Build udp header
    if (libnet_build_udp(sp,dp,LIBNET_UDP_H+payload_s,0, (u_int8_t*) payload,payload_s,*l,0) == -1)
    {
        fprintf(stderr, "Error building UDP header: %s\n",\
                libnet_geterror(*l));
        libnet_destroy(*l);
        exit(EXIT_FAILURE);
    }
    
    
    //Build IP header
    if (libnet_autobuild_ipv4(LIBNET_IPV4_H + LIBNET_UDP_H+\
                              payload_s,\
                              IPPROTO_UDP, dest_ip_addr, *l) == -1 )
    {
        fprintf(stderr, "Error building IP header: %s\n",\
                libnet_geterror(*l));
        libnet_destroy(*l);
        exit(EXIT_FAILURE);
    }
    
    write_packet(*l);
    return 0;
}

//getMyIP: given a context, returns numerical value for IP
u_int32_t getMyIP(libnet_t *l){
    u_int32_t my_ip_addr = libnet_get_ipaddr4(l);
    if(my_ip_addr == -1)    fprintf(stderr, "Couldn't get own IP address: %s\n",\
                                                libnet_geterror(l));
    return my_ip_addr;
}

//parse: given argc and argv, determines if arguments are for as server or client
//changes appropriate variables depending on the case (sp, dp, ip_addr)
int parse(int argc, char *argv[], u_int16_t* sp, u_int16_t* dp, char **ip_addr){
    char *ip_addr_str = NULL; //ip address is not an argument in the case of a client
    *dp = 22222; //default value for destination port
    *sp = 22222; //default value for source port
    if(argc<2){
        printf("Error! Usage is head_or_tail host [port] or head_or_tail -S [port]\n");
    } else {
        if(!strcmp(argv[1],"-S")) {
            if(argc>2) *sp = atoi(argv[2]);
            return SERVER;
        } else {
            ip_addr_str = argv[1];
            *ip_addr = ip_addr_str;
            if(argc>2) *dp = atoi(argv[2]);
            return CLIENT;
        }
    }
    printf("Error ocurred! Usage is head_or_tail host [port] or head_or_tail -S [port]\n");
    return -1; //if arrived here, an error ocurred
}

static void client(u_int16_t dp, char *ip_addr_str){
    //libnet --------------------------------------
    libnet_t *l; //the libnet context
    u_int32_t dest_ip_addr;
    u_int16_t sp; //source port
    //    ------------------------------------------
    initialize_context(&l); //initialize libnet context
    
    libnet_seed_prand(l); sp = libnet_get_prand(LIBNET_PR16); //porta origem escolhida aleatoriamente
    dest_ip_addr = libnet_name2addr4(l, ip_addr_str, LIBNET_DONT_RESOLVE); //valor numérico do endereço destino
    
    //pcap -----------------------------------------
    pcap_t *handle;		/* Session handle */
    char *pcap_dev, pcap_errbuf[PCAP_ERRBUF_SIZE];
    u_char *packet = NULL;		/* The actual packet */
    struct pcap_pkthdr header;	/* The header that pcap gives us */
    //----------------------------------------------
    initializeSniffer(&pcap_dev, pcap_errbuf, sp, &handle); //initializes "handle"
    
    //Printing useful client information
    printf("--------- Sobre o cliente ---------:\n");
    printf("IP: %s; ", libnet_addr2name4(getMyIP(l), LIBNET_DONT_RESOLVE));
    printf("Escutando porta UDP %u\n", sp);
    printf("-----------------------------------\n\n");
    
    
    //Etapa 1: Inicia comunicação
    printf("(1) Iniciar comunicação com o servidor");
    char *payload;
    payload = (char*) malloc(6*sizeof(char));
    strcpy(payload, "Hello");
    build_and_write(&l, payload, sizeof(payload), sp, dp, dest_ip_addr);
    printf("    IP do servidor: %s; Porta UDP: %u\n", ip_addr_str, dp);
    printf("    Enviando '%s' para o servidor\n",payload);
    
    //Etapa 2: começa a escutar em porta sp
    printf("(2) Aguardando contato do servidor...\n");
    libnet_clear_packet(l);
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL);
    
    free(payload);
    u_int16_t sp_aux; //something to placehold...
    u_int32_t dest_ip_addr_aux; //another place holder...
    disectPacket(packet, &sp_aux, &dest_ip_addr_aux, (u_int8_t **) &payload);
    
    printf("    Mensagem codificada recebida: 0x%x\n", *((u_int32_t* ) payload)); //I know I have 4 bytes received...
    u_int32_t encryptedMessage_server = *((u_int32_t* ) payload);
    
    //Etapa 3: enviar minha mensagem codificada para o servidor
    printf("(3) Preparando decisão para envio.\n");
    u_int32_t S; //chave secreta do cliente
    chooseHeadOrTail(&payload, &S); //payload é HEAD ou TAIL, S é a chave
    char *myChoice = payload;
    u_int32_t encryptedMessage = (*((u_int32_t*) payload)) ^ S; //mensagem codificada
    printf("    Decisão: %s;\n", payload);
    printf("    HEXA: 0x%x;\n", (*((u_int32_t*) payload)));
    printf("    Chave Secreta: 0x%x;\n", S);
    printf("    Código: 0x%x\n", encryptedMessage);
    libnet_clear_packet(l);
    printf("    Enviando código ao servidor\n");
    build_and_write(&l, (char *) (&encryptedMessage), sizeof(encryptedMessage), sp, dp, dest_ip_addr);
    
    //Etapa 4: receber chave secreta do servidor
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL);
    printf("(4) Aguardando chave secreta do servidor...\n");
    disectPacket(packet, &sp_aux, &dest_ip_addr_aux, ((u_int8_t**) &payload));
    u_int32_t S_server = *((u_int32_t* ) payload);
    printf("    Chave secreta recebida: 0x%x\n", S_server);
    
    u_int32_t decryptedMessage_server = encryptedMessage_server ^ S_server;
    char *decryptedString = (char*) malloc(5*sizeof(char));
    strcpy(decryptedString,(char *) (&decryptedMessage_server));
    decryptedString[4]='\0';
    printf("    Decodificando mensagem...\n");
    printf("    Mensagem decodificada: %s\n", decryptedString);
    
    //Etapa 5: enviar minha chave secreta
    printf("(5) Enviando chave secreta ao servidor.\n");
    libnet_clear_packet(l);
    build_and_write(&l, (char *) (&S), sizeof(S), sp, dp, dest_ip_addr);
    
    //Etapa 6: enviar OK ou NOK
    printf("(6) Preparando acusação para o servidor.\n");
    payload = (char*) malloc(4*sizeof(char));
    if(strcmp(myChoice, decryptedString)) strcpy(payload,"NOK"); //não iguais
    else strcpy(payload, "OK"); //iguais
    printf("    Enviando %s ao servidor.\n", payload);
    libnet_clear_packet(l);
    build_and_write(&l, (char *) payload, sizeof(payload), sp, dp, dest_ip_addr);
    
    //Etapa 7: receber NO ou NOK
    printf("(7) Aguardando acusação do servidor.\n");
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL);
    disectPacket(packet, &sp_aux, &dest_ip_addr_aux, ((u_int8_t**) &payload));
    printf("    Servidor disse %s\n", payload);
    
    //Etapa 8: receber Bye
    printf("(8) Aguardando última mensagem.\n");
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL);
    disectPacket(packet, &sp_aux, &dest_ip_addr_aux, ((u_int8_t**) &payload));
    printf("    Servidor disse %s\n", payload);
    printf("    Fim de serviço.\n");
    
    pcap_close(handle); //fechar sessão da libpcap
    libnet_destroy(l); //fechar sessão da libnet
    
}

static void server(u_int16_t sp){
    //libnet --------------------------------------
    libnet_t *l; //the libnet context
    u_int32_t dest_ip_addr;
    u_int16_t dp; //destination port
    char *payload;
    
    initialize_context(&l); //initialize libnet context

    //Printing useful server information
    printf("----------Sobre o servidor---------\n");
    printf("IP: %s; ", libnet_addr2name4(getMyIP(l), LIBNET_DONT_RESOLVE));
    printf("Escutando porta UDP %u\n", sp);
    printf("-----------------------------------\n\n");
    
    //pcap -----------------------------------------
    pcap_t *handle;		/* Session handle */
    char *pcap_dev, pcap_errbuf[PCAP_ERRBUF_SIZE];
    u_char *packet = NULL;		/* The actual packet */
    struct pcap_pkthdr header;	/* The header that pcap gives us */
    
    initializeSniffer(&pcap_dev, pcap_errbuf, sp, &handle); //initializes "handle"
    
    //Etapa 1: Aguardar contato do cliente
    printf("(1) Aguardando contato do cliente...\n");
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL); //faz nada com o pacote... só é importante ter recebido
    
    
    //Etapa 2: responder cliente
    disectPacket(packet, &dp, &dest_ip_addr, ((u_int8_t**) &payload));
    u_int32_t S;
    chooseHeadOrTail(&payload, &S);
    char *myChoice = payload;
    printf("(2) Preparando decisão.\n");
    u_int32_t encryptedMessage = (*((u_int32_t*) payload)) ^ S;
    printf("    Decisão: %s;\n", payload);
    printf("    HEXA: 0x%x;\n", (*((u_int32_t*) payload)));
    printf("    Chave Secreta: 0x%x;\n", S);
    printf("    Código: 0x%x\n", encryptedMessage);
    libnet_clear_packet(l);
    printf("    Enviando código ao cliente\n");
    build_and_write(&l, (char *) (&encryptedMessage), sizeof(encryptedMessage), sp, dp, dest_ip_addr);
    
    //Etapa 3: receber mensagem codificada do cliente
    printf("(3) Aguardando mensagem codificada do cliente\n");
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL);
    disectPacket(packet, &dp, &dest_ip_addr, ((u_int8_t**) &payload));
    u_int32_t encryptedMessage_client = *((u_int32_t* ) payload);
    printf("    Código 0x%x recebido do cliente\n", encryptedMessage_client); //I know I have 4 bytes received...
    
    //Etapa 4: enviar código secreto ao cliente
    libnet_clear_packet(l);
    printf("(4) Enviando chave secreta ao cliente\n");
    build_and_write(&l, (char *) (&S), sizeof(S), sp, dp, dest_ip_addr);
    
    //Etapa 5: receber código secreto do cliente
    printf("(5) Aguardando código secreto do cliente.\n");
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL);
    disectPacket(packet, &dp, &dest_ip_addr, ((u_int8_t**) &payload));
    u_int32_t S_client = *((u_int32_t* ) payload);
    printf("    Código secreto 0x%x recebido do cliente\n", S_client);
    
    //decoding
    printf("    Decodificando mensagem\n");
    u_int32_t decryptedMessage_client =encryptedMessage_client ^ S_client;
    char *decryptedString = (char*) malloc(5*sizeof(char));
    strcpy(decryptedString,(char *) (&decryptedMessage_client));
    decryptedString[4]='\0';
    printf("    Mensagem decodificada: %s\n", decryptedString);
    
    
    //Etapa 6: receber o OK ou NOK do cliente
    printf("(6) Aguardando acusação do cliente\n");
    do{
        packet = (u_char *) pcap_next(handle, &header);
    } while (packet==NULL);
    disectPacket(packet, &dp, &dest_ip_addr, ((u_int8_t**) &payload));
    char *isOK = payload;
    printf("    Cliente disse %s\n", isOK);
    
    //Etapa 7: enviar OK ou NOK
    printf("(7) Preparando acusação ao cliente\n");
    payload = (char*) malloc(4*sizeof(char));
    if(strcmp(myChoice,decryptedString)) strcpy(payload,"NOK"); //não iguais
    else strcpy(payload, "OK"); //iguais
    printf("    Enviando %s ao cliente.\n", payload);
    
    libnet_clear_packet(l);
    build_and_write(&l, (char *) payload, sizeof(payload), sp, dp, dest_ip_addr);
    
    //Etapa 8: enviar Bye
    strcpy(payload, "Bye");
    printf("(8) Enviando %s ao cliente.\n", payload);
    libnet_clear_packet(l);
    build_and_write(&l, (char *) payload, sizeof(payload), sp, dp, dest_ip_addr);
    printf("    Fim de serviço.\n");
    //Finalizando serviço
    pcap_close(handle); //fecha a sessão libpcap
    libnet_destroy(l); //fecha a sessão da libnet
    
}

void initializeSniffer(char **dev, char *errbuf, u_int16_t sp, pcap_t **handle){
    //Find standard device
    *dev = pcap_lookupdev(errbuf); //este dispositivo deve ser o mesmo que a libnet esteja usando
    if (*dev == NULL) {
        fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
        exit(EXIT_FAILURE);
    }
    
    //Prepare filter expression
    //Um filtro é utilizado para somente "espiar" dados que sejam
    //diretamente relacionados com o que o servidor/cliente esperam
    //Assim, tráfego de outras portas não é levado em consideração
    char port_no[20], filter_exp[20] = "udp dst port ";
    //após filtro, somente pacotes com porta destino port_no serão considerados
    sprintf(port_no, "%d", sp); //conversão de número pra string
    //definition of filter for sniffer
    strncat(filter_exp,port_no,10); //concatenando strings
    
    //abrir sessão, compilar e a aplicar filtro
    bpf_u_int32 mask;		/* Our netmask */
    bpf_u_int32 net;		/* Our IP */
    struct bpf_program fp;		/* The compiled filter */
    
    /* Find the properties for the device */
    if (pcap_lookupnet(*dev, &net, &mask, errbuf) == -1) {
        fprintf(stderr, "Couldn't get netmask for device %s: %s\n", *dev, errbuf);
        net = 0;
        mask = 0;
    }
    /* Abrir sessão em modo não-promíscuo */
    //o "0" indica modo não-promíscuo
    //o 1000 indica 1000ms de timeout
    *handle = pcap_open_live(*dev, BUFSIZ, 0, 1000, errbuf);
    if (*handle == NULL) {
        fprintf(stderr, "Couldn't open device %s: %s\n", *dev, errbuf);
        exit(EXIT_FAILURE);
    }
    /* Compilar e aplicar filtro */
    if (pcap_compile(*handle, &fp, filter_exp, 0, net) == -1) {
        fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(*handle));
        exit(EXIT_FAILURE);
    }
    if (pcap_setfilter(*handle, &fp) == -1) {
        fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(*handle));
        exit(EXIT_FAILURE);
    }
    
}

static void disectPacket(u_char *packet, u_int16_t *sp, u_int32_t *dest_ip_addr, u_int8_t **payload){
    struct sniff_ethernet *ethernet; /* The ethernet header */
    struct sniff_ip *ip; /* The IP header */
    struct sniff_udp *udp; /* The UDP header */
    u_int size_ip;
    
    //Casts são realizados de forma ao pacote tomar a "cara" desejada
    //seja de cabeçalho, seja de carga útil...
    ethernet = (struct sniff_ethernet*)(packet);
    ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
    size_ip = IP_HL(ip)*4;
    if (size_ip < 20) {
        printf("   * Comprimento de cabeçalho IP inválido: %u bytes\n", size_ip);
        return;
    }
    udp = (struct sniff_udp*)(packet + SIZE_ETHERNET + size_ip);
    *payload = (u_int8_t *)(packet + SIZE_ETHERNET + size_ip + SIZE_UDP);
    
    printf("    Pacote recebido ");
    printf("de origem %s, ", libnet_addr2name4(ip->ip_src, LIBNET_DONT_RESOLVE));
    *dest_ip_addr = ip->ip_src;
    printf("porta UDP %u.\n", UDP_CVT(udp->uh_sport));
    *sp = UDP_CVT(udp->uh_sport);
    printf("    Segmento UDP recebido tem %u bytes\n", UDP_CVT(udp->uh_ulen));
    
}

static void chooseHeadOrTail(char **choice, u_int32_t *S){
    int r = rand() % 2; //chooses HEAD (0) or TAIL (1)
    *choice = (char*) malloc(5*sizeof(char));
    if(r) strcpy(*choice, "TAIL");
    else strcpy(*choice, "HEAD");
    
    *S = (((rand() % 256) << 24) & 0xFF000000) | \
         (((rand() % 256) << 16) & 0x00FF0000) | \
         (((rand() % 256) <<  8) & 0x0000FF00) | \
         (((rand() % 256))       & 0x000000FF); //chooses secret key S
    
}